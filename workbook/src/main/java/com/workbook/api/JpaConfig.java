
package com.workbook.api;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * @author Zakir Hussain Syed
 * @created Dec 4, 2019 12:56:14 PM
 * @version 1.0
 * @filename JpaConfig.java
 * @package com.workbook.api 
 */

@Configuration
@EnableJpaAuditing
public class JpaConfig {

}
