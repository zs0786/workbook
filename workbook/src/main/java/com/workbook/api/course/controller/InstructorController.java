
package com.workbook.api.course.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.workbook.api.common.exception.ResourceNotFoundException;
import com.workbook.api.course.model.Instructor;
import com.workbook.api.course.repository.InstructorRepository;

/**
 * @author Zakir
 *
 */

@RestController
@RequestMapping("/api/v1")
public class InstructorController {
	
	@Autowired
    private InstructorRepository repository;


    @GetMapping("/instructors")
    public List <Instructor> getInstructors() {
        return repository.findAll();
    }

    @GetMapping("/instructors/{id}")
    public ResponseEntity <Instructor> getInstructorById(@PathVariable(value = "id") int instructorId) throws ResourceNotFoundException {
        Instructor user = repository.findById(instructorId)
            .orElseThrow(() -> new ResourceNotFoundException("Instructor not found :: " + instructorId));
        return ResponseEntity.ok().body(user);
    }
    
    @GetMapping("/instructors/byids")
    public ResponseEntity<List<Instructor>> getAllByIds(@RequestBody int ids) throws ResourceNotFoundException {
    	
    	List<Instructor> instructors = repository.findAllById(ids);
    	return ResponseEntity.ok().body(instructors);
    }

    @PostMapping("/instructors")
    public Instructor createUser(@Valid @RequestBody Instructor instructor) {
        return repository.save(instructor);
    }

    @PutMapping("/instructors/{id}")
    public ResponseEntity <Instructor> updateUser(@PathVariable(value = "id") int instructorId, @Valid @RequestBody Instructor userDetails) 
    		throws ResourceNotFoundException {
        Instructor user = repository.findById(instructorId)
            .orElseThrow(() -> new ResourceNotFoundException("Instructor not found :: " + instructorId));
        user.setFirstName(userDetails.getFirstName());
        user.setLastName(userDetails.getLastName());
        user.setEmail(userDetails.getEmail());
        final Instructor updatedUser = repository.save(user);
        return ResponseEntity.ok(updatedUser);
    }

    @DeleteMapping("/instructors/{id}")
    public Map <String, Boolean> deleteUser(@PathVariable(value = "id") int instructorId) throws ResourceNotFoundException {
        Instructor instructor = repository.findById(instructorId)
            .orElseThrow(() -> new ResourceNotFoundException("Instructor not found :: " + instructorId));

        repository.delete(instructor);
        Map <String, Boolean> response = new HashMap<> ();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

}
