
package com.workbook.api.course.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.workbook.api.course.model.Course;

/**
 * @author Zakir
 *
 */

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
	
	 List<Course> findByInstructorId(int instructorId);
	 
	 Optional<Course> findByIdAndInstructorId(int id, int instructorId);
	 
	 Optional<Course> findById(int courseId);

}
