
package com.workbook.api.course.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.workbook.api.course.model.Instructor;

/**
 * @author Zakir
 *
 */

@Repository
public interface InstructorRepository extends JpaRepository<Instructor, Long> {

	List<Instructor> findAllById(int ids);

	Optional<Instructor> findById(int instructorId);

	boolean existsById(int instructorId);

}
