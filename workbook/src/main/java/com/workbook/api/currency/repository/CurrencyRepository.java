
package com.workbook.api.currency.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.workbook.api.currency.model.Currency;

/**
 * @author Zakir
 *
 */

@Repository
public interface CurrencyRepository extends CrudRepository<Currency, Long> {

}
