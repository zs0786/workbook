package com.workbook.api.roughwork;

import java.sql.Timestamp;
import java.text.Format;
import java.text.SimpleDateFormat;
//import java.util.Date;

/**
 * @author Zakir Hussain Syed
 * @created Dec 17, 2019 11:01:51 AM
 * @version 1.0
 * @filename GetMonthFromTimestamp.java
 * @package com.workbook.api.roughwork 
 */
public class GetMonthFromTimestamp {
	
	public static void main(String[] argv) throws Exception {

	    Format formatter = new SimpleDateFormat("MMMM"); 
	    String s = formatter.format(new Timestamp(System.currentTimeMillis()));
	    System.out.println(s);
	}
}
