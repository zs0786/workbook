
package com.workbook.api.user.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Zakir
 *
 */

@Entity    
@Getter    
@Setter
public class User {
	
	@Id 
	 @GeneratedValue(strategy = GenerationType.AUTO)    
	 @Column(nullable = false, updatable = false)    
	 private Long id;    
	    
	 private String userName;    
	 private String password;

	 public User(String userName, String password) {    
	        this.userName = userName;    
	        this.password = password;    
	 }    
	    
	 public User() {}
}
