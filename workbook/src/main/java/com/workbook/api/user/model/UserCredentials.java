
package com.workbook.api.user.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Zakir
 *
 */

@Getter  
@Setter 
public class UserCredentials {
	
	private String userName;  
    private String password;

}
