package com.workbook.api.user.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.workbook.api.user.model.User;

/**
 * @author Zakir
 *
 */

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
	
	Optional<User> findByUserName(String username); 

}
