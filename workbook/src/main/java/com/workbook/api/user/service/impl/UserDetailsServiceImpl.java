
package com.workbook.api.user.service.impl;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.workbook.api.user.model.User;
import com.workbook.api.user.repository.UserRepository;

import org.springframework.stereotype.Service;

/**
 * @author Zakir
 *
 */

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	
	 private UserRepository userRepository;
    
	 @Autowired    
	 public UserDetailsServiceImpl(UserRepository userRepository) {    
	        this.userRepository = userRepository;    
	    }  
	 
	 /* (non-Javadoc)
	  * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
	  */   
	 
	 @Override 
	 public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {    
	        User user = userRepository.findByUserName(userName)    
	                .orElseThrow(() -> new UsernameNotFoundException("User: " + userName + " not found"));    
	        return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(),    
	                Arrays.asList(new SimpleGrantedAuthority("user")));    
	    }

}
